# Webcounter

Simple Python Webcounter with redis 

## Build
docker build -t webcounter .

## Dependencies
docker run -d  --name redis --rm redis:alpine

## Run
docker run -d --rm -p80:5000 --name webcounter --link redis -e REDIS_URL=redis webcounter

